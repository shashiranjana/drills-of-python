# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 09:24:11 2019

@author: shash
"""
start=8
end=5
step=-1

def integers_from_start_to_end_using_range(start, end, step):
    """return a list"""
    listVal=[]
    for i in range(start, end, step):
        listVal.append(i)
    print(listVal)
    return listVal


def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    listVal=[]
    if start<end:
        while start< end:
            listVal.append(start)
            start+=step
    else:
        while start> end:
            listVal.append(start)
            start+=step
    print(listVal)
    return listVal

def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    listOfPrime=[]
    for i in range (10,100):
        count=False
        for j in range(2,i):
            if i%j==0:
                count=True
                
        if count== False:
            listOfPrime.append(i)
            
    print(listOfPrime)        
            
    
    
  

two_digit_primes()