# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 18:21:23 2019

@author: shash
"""
n=10
digit=2
def is_prime(n):
    """
    Check whether a number is prime or not
    """
    check=False
    for i in range(2,n):
        if n%i==0:
            check=True
    if check==False:
        print("It is prime",n)
        
def n_digit_primes(digit):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    start =10**(digit-1)
    end=10**digit
    for j in range(start,end):
        is_prime(j)
        
    
n_digit_primes(digit)