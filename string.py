# -*- coding: utf-8 -*-
"""
Created on Sat Aug 31 15:06:05 2019

@author: shash
"""
x="abcd"
def last_3_characters(x):
    print(x[-3:])


def first_10_characters(x):
    print(x[:10])


def chars_4_through_10(x):
    print(x[4:10])


def str_length(x):
    print(len(x))


def words(x):
    print(list(x))


def capitalize(x):
    print(x.capitalize())


def to_uppercase(x):
    print(x.upper())

