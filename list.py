# -*- coding: utf-8 -*-
"""
Created on Sat Aug 31 15:19:06 2019

@author: shash
"""
x1=['a','c','b']
x= [41, 5, 6]
def sum_items_in_list(x):
    print(sum(x))


def list_length(x):
    print(len(x))


def last_three_items(x):
    print(x[-3:])


def first_three_items(x):
    print(x[:3])


def sort_list(x):
    x.sort()
    print(x)


def append_item(x, item):
    x.append(item)
    print(x)


def remove_last_item(x):
    del x[-1]
    print(x)

def count_occurrences(x, item):
    print(x.count(item))


def is_item_present_in_list(x, item):
    if(x.count(item)>0):
        print("Exist")
    else:
        print("Don't Exist")

def append_all_items_of_y_to_x(x, y):
    """
    x and y are lists
    """
    x.extend(y)
    print(x)


def list_copy(x):
    """
    Create a shallow copy of x
    """
    print(x.copy())
sort_list(x)